<?php

namespace App\Form;

use App\Entity\Mairie;
use App\Entity\Parti;
use App\Entity\Politicien;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewPoliticienType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('sexe')
            ->add('age')
            ->add('Mairie', EntityType::class, [
                'class' => Mairie::class,
                'choice_label' => 'ville',
            ])
            ->add('Parti', EntityType::class,[
                'class'=> Parti::class,
                'choice_label' => 'nom',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Politicien::class,
        ]);
    }
}

<?php


namespace App\Controller;


use App\Entity\Affaire;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    function index(){
        return $this->render('index.html.twig');
    }


    /**
     * @Route("/menu", name="menu")
     */
    function menu(){
        return $this->render('menu.html.twig');
    }

    /**
     * @Route("/recherche", name="search_result", methods={"GET"})
     */
    public function search(Request $request): Response
    {
        $search = $request->query->all();

        $res = $this->getDoctrine()->getRepository(Affaire::class)->findAllByDesignation($search["search"]);

        return $this->render('affaire/index.html.twig', [
            'affaires' => $res,
        ]);
    }
}
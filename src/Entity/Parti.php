<?php

namespace App\Entity;

use App\Repository\PoliticienRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartiRepository")
 * @UniqueEntity(fields={"nom"}, message="Ce parti existe déjà.")
 */
class Parti
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $ID;

    /**
     * @ORM\Column(type="string", length=255, unique = true)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Politicien", mappedBy="parti")
     */
    private $politiciens;

    public function __construct()
    {
        $this->politiciens = new ArrayCollection();
    }

    public function getID(): ?int
    {
        return $this->ID;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getParite(): int{
        $homme = 0;
        $politiciens = $this->getPoliticiens();

        if(count($politiciens) == 0 )
            return -1;

        foreach($politiciens as $politicien){
            if($politicien->getSexe() === "M")
                $homme++;
        }
        return ceil($homme/count($politiciens)*100);
    }

    public function getAgeMoyen(){
        $ageTotal = 0;
        $politiciens = $this->getPoliticiens();

        if(count($politiciens) == 0 )
            return -1;
        foreach($politiciens as $politicien){
            $ageTotal += $politicien->getAge();
        }
        return ($ageTotal/count($politiciens));
    }

    /**
     * @return Collection|Politicien[]
     */
    public function getPoliticiens(): Collection
    {
        return $this->politiciens;
    }

    public function addPoliticien(Politicien $politicien): self
    {
        if (!$this->politiciens->contains($politicien)) {
            $this->politiciens[] = $politicien;
            $politicien->setParti($this);
        }

        return $this;
    }

    public function removePoliticien(Politicien $politicien): self
    {
        if ($this->politiciens->contains($politicien)) {
            $this->politiciens->removeElement($politicien);
            // set the owning side to null (unless already changed)
            if ($politicien->getParti() === $this) {
                $politicien->setParti(null);
            }
        }

        return $this;
    }
}
